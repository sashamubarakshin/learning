/**
 *  Config by ALM
 *  Current version webpack - 2
 * 
 */
const path = require("path");
const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const ENV_PROD = process.argv.includes('--env=prod');
console.log(`Is ${ENV_PROD ? "" : "NOT "}production`);

module.exports = {
    entry: {
        "app": "./src/app.tsx",
    },
    output: {
        path: path.resolve(__dirname, "./public/build"),
        filename: "[name].js"
    },

    resolve: {
        extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
    },

    module: {
        loaders: [
            // All files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'.
            {
                test: /\.tsx?$/,
                loader: "ts-loader"
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': ENV_PROD ? JSON.stringify('production') :
                    JSON.stringify('development')
            }
        }),
        new webpack.LoaderOptionsPlugin({
            debug: !ENV_PROD,
            minimize: ENV_PROD,
            options: {
                minimize: ENV_PROD,
                debug: !ENV_PROD,
                resolve: {}
            }
        }),
        // ENV_PROD ? new webpack.optimize.UglifyJsPlugin({}) : function () {}
    ]
};