import * as React from "react";
import * as ReactDOM from "react-dom";
import { hashHistory, Route, Router, Redirect } from "react-router";

// Forms
import { App } from "./scenes/main";
import { Index } from "./scenes/index";
import { Video } from "./scenes/video";


import { Lectures } from "./scenes/lectures";
import { Lectures_1 } from "./scenes/first/lectures-1";
import { Lectures_1_2 } from "./scenes/first/lectures-1.2";
import { Lectures_1_3 } from "./scenes/first/lectures-1.3";

import { Lectures_2 } from "./scenes/second/lectures-2";
import { Lectures_2_2 } from "./scenes/second/lectures-2.2";
import { Lectures_2_3 } from "./scenes/second/lectures-2.3";
import { Lectures_2_4 } from "./scenes/second/lectures-2.4";

import { Lectures_3 } from "./scenes/third/lectures-3";
import { Lectures_3_2 } from "./scenes/third/lectures-3.2";
import { Lectures_3_3 } from "./scenes/third/lectures-3.3";
import { Lectures_3_4 } from "./scenes/third/lectures-3.4";

import { Lectures_4 } from "./scenes/fourth/lectures-4";
import { Lectures_4_2 } from "./scenes/fourth/lectures-4.2";
import { Lectures_4_3 } from "./scenes/fourth/lectures-4.3";

import { Lectures_5 } from "./scenes/fifth/lectures-5";
import { Lectures_5_2 } from "./scenes/fifth/lectures-5.2";
import { Lectures_5_3 } from "./scenes/fifth/lectures-5.3";
import { Lectures_5_4 } from "./scenes/fifth/lectures-5.4";

import { Lectures_6 } from "./scenes/sixth/lectures-6";
import { Lectures_6_2 } from "./scenes/sixth/lectures-6.2";
import { Lectures_6_3 } from "./scenes/sixth/lectures-6.3";
import { Lectures_6_4 } from "./scenes/sixth/lectures-6.4";

import { Pract } from "./scenes/pract/pract";
import { Pract_1 } from "./scenes/pract/pract-1";
import { Pract_2 } from "./scenes/pract/pract-2";
import { Pract_3 } from "./scenes/pract/pract-3";
import { Pract_4 } from "./scenes/pract/pract-4";
import { Pract_5 } from "./scenes/pract/pract-5";

import { Test } from "./scenes/tests/test-scene";

const test_1 = () => (<Test number='1' />);
const test_2 = () => (<Test number='2' />);
const test_3 = () => (<Test number='3' />);
const test_4 = () => (<Test number='4' />);


const routes = (
    <Router history={hashHistory}>
        <Redirect from="/" to="index" />
        <Route path="/" component={App}>
            <Route path="/index" component={Index} />
            <Route path="/video" component={Video} />

            <Route path="/test-1" component={test_1} />
            <Route path="/test-2" component={test_2} />
            <Route path="/test-3" component={test_3} />
            <Route path="/test-4" component={test_4} />

            <Route path="/lectures" component={Lectures} />
            <Route path="/lectures-1" component={Lectures_1} />
            <Route path="/lectures-1.2" component={Lectures_1_2} />
            <Route path="/lectures-1.3" component={Lectures_1_3} />

            <Route path="/lectures-2" component={Lectures_2} />
            <Route path="/lectures-2.2" component={Lectures_2_2} />
            <Route path="/lectures-2.3" component={Lectures_2_3} />
            <Route path="/lectures-2.4" component={Lectures_2_4} />

            <Route path="/lectures-3" component={Lectures_3} />
            <Route path="/lectures-3.2" component={Lectures_3_2} />
            <Route path="/lectures-3.3" component={Lectures_3_3} />
            <Route path="/lectures-3.4" component={Lectures_3_4} />

            <Route path="/lectures-4" component={Lectures_4} />
            <Route path="/lectures-4.2" component={Lectures_4_2} />
            <Route path="/lectures-4.3" component={Lectures_4_3} />

            <Route path="/lectures-5" component={Lectures_5} />
            <Route path="/lectures-5.2" component={Lectures_5_2} />
            <Route path="/lectures-5.3" component={Lectures_5_3} />
            <Route path="/lectures-5.4" component={Lectures_5_4} />

            <Route path="/lectures-6" component={Lectures_6} />
            <Route path="/lectures-6.2" component={Lectures_6_2} />
            <Route path="/lectures-6.3" component={Lectures_6_3} />
            <Route path="/lectures-6.4" component={Lectures_6_4} />

            <Route path="/pract" component={Pract} />
            <Route path="/pract-1" component={Pract_1} />
            <Route path="/pract-2" component={Pract_2} />
            <Route path="/pract-3" component={Pract_3} />
            <Route path="/pract-4" component={Pract_4} />
            <Route path="/pract-5" component={Pract_5} />

            <Route path="*" component={Index} />
        </Route>
    </Router>
);

ReactDOM.render(routes, document.getElementById("app"));
