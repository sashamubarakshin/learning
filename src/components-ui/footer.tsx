import * as React from "react";

export class Footer extends React.Component<{}, {}> {
    render() {
        return (

            <footer id="footWrapper">

                <div className="footer-middle">
                    <div className="container">
                        <div className="row">

                            <div className="col-md-4 fx">
                                <h3>О нас</h3>
                                <p className="foot-about-par" >
                                    Данный курс является продолжением курса "Основы программирования". Вы освоите самую распространенную и востребованную парадигму, используемую практически во всех современных языках - объектно ориентированное программирование.</p>
                            </div>

                            <div className="col-md-4 fx last contact-widget">
                                <h3>Свяжитесь с нами</h3>
                                <ul className="details">
                                    <li><i className="fa fa-map-marker"></i><span>ул. Машиностроителей,15 г.Йошкар-Ола, Россия.</span></li>
                                    <li><i className="fa fa-envelope"></i><span>alm@digt.ru</span></li>
                                    <li><i className="fa fa-phone"></i><span>+7(917)710-41-43</span></li>
                                </ul>
                                <div className="foot-newletters">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </footer>
        )
    }
}