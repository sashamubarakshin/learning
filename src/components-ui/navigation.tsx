import * as React from "react";

export class NavigationHeader extends React.Component<{}, {}> {
    render() {
        return (
            <header className="top-head header-1 skew">
                <div className="container">
                    <div className="logo">
                        <a href="#/index"><img alt="" src="assets/images/logo.png" /></a>
                    </div>

                    <div className="responsive-nav">
                        <nav className="top-nav">
                            <ul>
                                <li><a href="#/index"><span>Главная</span></a></li>
                                <li className="selected hasChildren"><a href="#/lectures"><span>Лекции</span></a>
                                    <ul>
                                        <li className="hasChildren" ><a href="#/lectures-1"><i className="fa fa-share"></i>1. Общие сведения</a>
                                            <ul>
                                                <li ><a href="#/lectures-1">1.1. Для чего нужно объектно-ориентированное программирование?</a></li>
                                                <li><a href="#/lectures-1.2">1.2. Объектно-ориентированный подход1</a></li>
                                                <li><a href="#/lectures-1.3">1.3. Характеристики объектно-ориентированных языков</a></li>
                                                <li><a href="#/lectures-1.4">1.4. C++ и С</a></li>
                                                <li><a href="#/lectures-1.5">1.5. Изучение основ</a></li>
                                            </ul>
                                        </li>
                                        <li className="hasChildren" ><a href="#/lectures-2"><i className="fa fa-share"></i>2. Основы прог. на С++</a>
                                            <ul>
                                                <li><a href="#/lectures-2.1">2.1. Структура программы</a></li>
                                                <li><a href="#/lectures-2.2">2.2. Директивы</a></li>
                                                <li><a href="#/lectures-2.3">2.3. Комментарии</a></li>
                                                <li><a href="#/lectures-2.4">2.4. Переменные</a></li>
                                            </ul>
                                        </li>
                                        <li className="hasChildren" ><a href="#/lectures-3"><i className="fa fa-share"></i>3. Циклы и ветвления</a>
                                            <ul>
                                                <li><a href="#/lectures-3">3.1. Циклы</a></li>
                                                <li><a href="#/lectures-3.2">3.2. Ветвления</a></li>
                                                <li><a href="#/lectures-3.3">3.3. Логические операции</a></li>
                                                <li><a href="#/lectures-3.4">3.4. Приоритеты операций C++</a></li>
                                            </ul>
                                        </li>
                                        <li ><a href="#/lectures-4"><i className="fa fa-share"></i>4. Структуры</a></li>
                                        <li className="hasChildren" ><a href="#/lectures-5"><i className="fa fa-share"></i>5. Функции</a>
                                            <ul>
                                                <li><a href="#/lectures-5">5.1. Простые функции</a></li>
                                                <li><a href="#/lectures-5.2">5.2. Передача аргументов в функцию</a></li>
                                                <li><a href="#/lectures-5.3">5.3. Значение, возвращаемое функцией</a></li>
                                                <li><a href="#/lectures-5.4">5.4. Перегруженные функции</a></li>
                                                <li><a href="#/lectures-5.5">5.5. Рекурсия</a></li>
                                            </ul>
                                        </li>
                                        <li className="hasChildren" ><a href="#/lectures-6"><i className="fa fa-share"></i>6. Объекты и классы</a>
                                            <ul>
                                                <li><a href="#/lectures-6">6.1. Простой класс</a></li>
                                                <li><a href="#/lectures-6.2">6.2. Конструкторы</a></li>
                                                <li><a href="#/lectures-6.3">6.3. Структуры и классы</a></li>
                                                <li><a href="#/lectures-6.4">6.4.Классы, объекты и память</a></li>
                                                <li><a href="#/lectures-6.5">6.5. Статические данные класса</a></li>
                                                <li><a href="#/lectures-6.6">6.6. Зачем нужны классы?</a></li>
                                            </ul>
                                        </li>

                                    </ul>
                                </li>
                                <li className="hasChildren"><a href="#/pract"><span>Практика</span></a>
                                    <ul>
                                        <li><a href="#/pract-1">1. Класс в С++</a></li>
                                        <li><a href="#/pract-2">2. Специальные функции-элементы класса </a></li>
                                        <li><a href="#/pract-3">3. Друзья класса. Перегрузка методов</a></li>
                                        <li><a href="#/pract-4">4. Наследование классов</a></li>
                                        <li><a href="#/pract-5">5. Параметризированные классы</a></li>

                                    </ul>
                                </li>
                                <li className="hasChildren"><a href="#/test-1"><span>Тесты</span></a>
                                    <ul>
                                        <li><a href="#/test-1">1. Общие сведения</a></li>
                                        <li><a href="#/test-2">2. Основы прог. на С++</a></li>
                                        <li><a href="#/test-3">3. Циклы и ветвления</a></li>
                                        <li><a href="#/test-4">4. Структуры</a></li>
                                    </ul>                                </li>
                                <li><a href="#/video"><span>Видеокурс</span></a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </header>
        )
    }
}