import * as React from "react";

import { NavigationHeader } from "./navigation";
import { Footer } from "./footer";

interface IAppMainProps { }

export class AppMain extends React.Component<IAppMainProps, {}> {
    render() {
        return (
            <div className="pageWrapper">
                <NavigationHeader />

                <div className="pageContent">
                    {this.props.children}
                </div>

                <Footer />
            </div>
        );
    }
}