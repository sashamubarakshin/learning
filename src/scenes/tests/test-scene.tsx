import * as React from "react";

interface TestHeaderProps {
    number: string;
}

class TestHeader extends React.PureComponent<TestHeaderProps, {}> {
    render() {
        return (
            <div>
                <div className="page-title grad-desk heading main  t-center">
                    <div>
                        <h3 className="uppercase lg-title ">Тест-
						<span className="main-color">№{this.props.number}</span>
                        </h3>
                    </div>
                </div>

                <div className="breadcrumbs">
                    <div className="container">
                        <a href="#">Главня</a>
                        <i className="fa fa-long-arrow-right main-color"></i>
                        <span>Тесты</span>
                    </div>
                </div>
            </div>
        )
    }
}

interface TestProps {
    number?: string;
}

export class Test extends React.Component<TestProps, {}> {
    render() {
        return (
            <div>
                <TestHeader number={this.props.number} />
                <iframe src={`./assets/templates/test-${this.props.number}.html`} />
            </div>
        )
    }
}