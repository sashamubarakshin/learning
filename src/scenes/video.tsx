import * as React from "react";

export class Video extends React.Component<{}, {}> {
    render() {
        return (
            <div>
                
	    		<div className="page-title grad-desk heading main  t-center">
					<div>
						<h3 className="uppercase lg-title ">Видео<span className="main-color">курс</span></h3>
					</div>
				</div>
				
				<div className="breadcrumbs">
					<div className="container">
						<a href="#">Главня</a><i className="fa fa-long-arrow-right main-color"></i><span>Видеокурс</span>
					</div>
				</div>

				<div className="container grad-desk">
	    			<div className="row row-eq-height">
	    				<div className="col-md-12 md-padding main-content">
                            
						<div className="row">
		    				<div className="col-md-4 fx">
		    					<h3>Как создать программу</h3>
		    					<div className="embed-responsive embed-responsive-16by9">
								    <video poster="./video/1.jpg" controls>
							            <source src='./video/Уроки по c . 1 урок.Как создать программу.mp4' type='./video/mp4' />
							        </video>
								</div>	
							    <p>Данное видео находится на сайте <a className="more_btn main-color" href="https://www.youtube.com/watch?v=MOpF5lk3cLY">Youtube</a></p>
						    </div>
							<div className="col-md-4 fx">
		    					<h3>Переменные</h3>
		    					<div className="embed-responsive embed-responsive-16by9">
								    <video poster="./video/2.jpg" controls>
							            <source src='./video/Уроки по c . 2 урок.Переменные и ввод с клавиатуры.mp4' type='./video/mp4' />
							        </video>
								</div>	
							    <p>Данное видео находится на сайте <a className="more_btn main-color" href="">Youtube</a></p>
						    </div>
							<div className="col-md-4 fx">
		    					<h3>Типы данных</h3>
		    					<div className="embed-responsive embed-responsive-16by9">
								    <video poster="./video/3.jpg" controls>
							            <source src='./video/Уроки по c . 3 урок.Типы данных.mp4' type='./video/mp4' />
							        </video>
								</div>	
							    <p>Данное видео находится на сайте <a className="more_btn main-color" href="">Youtube</a></p>
						    </div>
							<div className="col-md-4 fx">
		    					<h3>If-Else, Switch</h3>
		    					<div className="embed-responsive embed-responsive-16by9">
								    <video poster="./video/4.jpg" controls>
							            <source src='video/Уроки по c . 4 урок.If-Else, Switch.mp4' type='video/mp4' />
							        </video>
								</div>	
							    <p>Данное видео находится на сайте <a className="more_btn main-color" href="">Youtube</a></p>
						    </div>
							<div className="col-md-4 fx">
		    					<h3>Циклы</h3>
		    					<div className="embed-responsive embed-responsive-16by9">
								    <video poster="video/5.jpg" controls>
							            <source src='video/Уроки по c . 5 урок.Циклы.mp4' type='video/mp4' />
							        </video>
								</div>	
							    <p>Данное видео находится на сайте <a className="more_btn main-color" href="">Youtube</a></p>
						    </div>
							<div className="col-md-4 fx">
		    					<h3>Массивы</h3>
		    					<div className="embed-responsive embed-responsive-16by9">
								    <video poster="video/6.jpg" controls>
							            <source src='video/Уроки по c . 6 урок. Массивы.mp4' type='video/mp4' />
							        </video>
								</div>	
							    <p>Данное видео находится на сайте <a className="more_btn main-color" href="">Youtube</a></p>
						    </div>
							<div className="col-md-4 fx">
		    					<h3>Строки и 2 мерный массив</h3>
		    					<div className="embed-responsive embed-responsive-16by9">
								    <video poster="video/7.jpg" controls>
							            <source src='video/Уроки по c . 7 урок. Строки и 2 мерный массив.mp4' type='video/mp4' />
							        </video>
								</div>	
							    <p>Данное видео находится на сайте <a className="more_btn main-color" href="">Youtube</a></p>
						    </div>
							<div className="col-md-4 fx">
		    					<h3>Указатели</h3>
		    					<div className="embed-responsive embed-responsive-16by9">
								    <video poster="video/8.jpg" controls>
							            <source src='video/Уроки по c . 8 урок. Указатели.mp4' type='video/mp4' />
							        </video>
								</div>	
							    <p>Данное видео находится на сайте <a className="more_btn main-color" href="">Youtube</a></p>
						    </div>
							<div className="col-md-4 fx">
		    					<h3>Define, typedef, enum...</h3>
		    					<div className="embed-responsive embed-responsive-16by9">
								    <video poster="video/9.jpg" controls>
							            <source src='video/Уроки по c . 9 урок. Define, typedef, enum, rand(), Битовые операции.mp4' type='video/mp4' />
							        </video>
								</div>	
							    <p>Данное видео находится на сайте <a className="more_btn main-color" href="">Youtube</a></p>
						    </div>
							<div className="col-md-4 fx">
		    					<h3>Функции</h3>
		    					<div className="embed-responsive embed-responsive-16by9">
								    <video poster="video/10.jpg" controls>
							            <source src='video/Уроки по c . 10 урок. Функции.mp4' type='video/mp4' />
							        </video>
								</div>	
							    <p>Данное видео находится на сайте <a className="more_btn main-color" href="">Youtube</a></p>
						    </div>
							
							
		    			</div>
					
							
	    				</div>
	    			</div>
				</div>
            </div>
        )
    }
}