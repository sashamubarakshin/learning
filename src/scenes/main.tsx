import * as React from "react";

// Components UI
import { AppMain } from "../components-ui/app-main";

interface IAppProps { }

interface IAppState { }

export class App extends React.Component<IAppProps, IAppState> {

    constructor(props: IAppProps) {
        super(props);
        this.state = {};
    }

    render() {

        return (
            <AppMain>
                {this.props.children}
            </AppMain >
        );
    }
}