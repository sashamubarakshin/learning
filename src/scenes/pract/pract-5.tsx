import * as React from "react";

export class Pract_5 extends React.Component<{}, {}> {
    render() {
        return (
            <div>
                
                
	    		<div className="page-title grad-desk">
					<div className="container">
						<h1>Лабораторное занятие №5. Параметризированные классы. </h1>
					</div>
				</div>
				
				<div className="container">
                    <div className=" t-center">
		    			<ul className="pagination style2">
						    <li><a href="#/pract">Практика</a></li>
							<li><a href="#/pract-4" aria-label="Previous"><span aria-hidden="true"><i className="fa fa-long-arrow-left"></i></span></a></li>
							<li><a href="#/pract-1">1</a></li>
							<li><a href="#/pract-2">2</a></li>
							<li><a href="#/pract-3">3</a></li>
							<li><a href="#/pract-4">4</a></li>
							<li className="active"><a href="#/pract-5">5</a></li>
						</ul>
					</div>
                </div>
				
				<div className="breadcrumbs">
					<div className="container">
						<a href="#">Главня</a><i className="fa fa-long-arrow-right main-color"></i><a href="#">Практика</a><i className="fa fa-long-arrow-right main-color"></i><span>5. Параметризированные классы </span>
					</div>
				</div>

				<div className="container ">
	    			<div className="row row-eq-height">
	    				<div className="col-md-12 md-padding main-content">
						    
							<h3>Параметризированные классы. </h3> 
                            <p style={{textIndent: 25}}>Параметризированный класс (шаблон класса) даёт обобщённое определение семейства классов, использующее произвольные типы и константы. Параметризированный класс определяет общий класс, который может быть применён к изменяющимся типам данных; конкретный тип данных, над которым выполняются операции, передаётся в качестве параметра. Наиболее широкое применение шаблоны классов находят при создании контейнерных классов – классы, в которых хранятся организованные данные. Например, массивы, связанные списки, деревья и т.д. Определив в параметризированном классе логику, он может быть применён к любому типу данных. </p>
							<p style={{textIndent: 25}}>Определение шаблона класса </p>
							<p style={{textIndent: 25}}>Общая форма декларации шаблона класса: </p>
							<p>template{` <список аргументов шаблона> `}class Имя класса <br/>{`{`} <br/>	 	// Тело класса <br/>}; <br/></p>
							<p style={{textIndent: 25}}>За ключевым словом template следуют один или несколько аргументов, заключённых в угловые скобки и отделяемых друг от друга запятыми. Каждый аргумент является: </p>
							<p>•	Либо именем типа, за которым следует идентификатор. <br/>•	Либо ключевым словом class, за которым следует идентификатор, обозначающий параметризированный тип. <br/></p>
							<p className=" t-center"><img src="images/p/20.png" /></p>
							<h3>Задание №1  </h3> 
                            <p style={{textIndent: 25}}>Определите, 	используя 	 	параметризованный 	класс 	«Vector», параметризованный  класс «Очередь». Определите методы занесения данных в очередь и извлечения данных. Проверьте класс для встроенного типа «char» и  класса «Point». </p>
							<h3>Задание №2 </h3> 
                            <p style={{textIndent: 25}}>Разработайте параметризованный класс на основе класса «Vector» класс «Matrix» (матрица) .  </p>
							<h3>Задание №3 </h3> 
                            <p style={{textIndent: 25}}>Разработайте параметризованный класс «Двунаправленный список».  </p>
							<h3>Задание №4 </h3> 
                            <p style={{textIndent: 25}}>Разработайте параметризованный класс «Разряженный массив».  </p>
				
                            
	    				</div>
	    			</div>
				</div>

            </div>
        )
    }
}